<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/entry', 'EntryController@create')->name('entry');
    Route::get('/entry/{entryid}', 'EntryController@view');

    // Importer Routes
    Route::get('/importer/create', 'ImporterController@create')->name('importer.create');
    Route::get('/importer/{id}/edit', 'ImporterController@edit')->name('importer.edit');
    Route::get('/importer/{id}/detail', 'ImporterController@show')->name('importer.show');

    // Law Route
    Route::get('/law/create', 'LawController@create')->name('law.create');
    Route::get('/law/{id}/edit', 'LawController@edit')->name('law.edit');
    Route::get('/law/{id}/detail', 'LawController@show')->name('law.show');


    Route::group(['middleware' => 'admin'], function(){
        // User Routes
        Route::get('/user/create', 'UserController@create')->name('user.create');
        Route::get('/user/{id}/edit', 'UserController@edit')->name('user.edit');
    });
});
