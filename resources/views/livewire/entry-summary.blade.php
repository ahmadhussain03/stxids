<form wire:submit.prevent="save">
  <div class="errors">
@if ($errors->any())
<!--
  Tailwind UI components require Tailwind CSS v1.8 and the @tailwindcss/ui plugin.
  Read the documentation to get started: https://tailwindui.com/documentation
-->
<div class="rounded-md bg-red-50 p-4">
  <div class="flex">
    <div class="flex-shrink-0">
      <!-- Heroicon name: x-circle -->
      <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
        <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
      </svg>
    </div>
    <div class="ml-3">
      <h3 class="text-sm leading-5 font-medium text-red-800">
        There were errors with your submission
      </h3>
      <div class="mt-2 text-sm leading-5 text-red-700">
        <ul class="list-disc pl-5">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    </div>
  </div>
</div>

@endif
    @if(session('success'))
    <div class="rounded-md bg-green-50 p-4">
        <div class="flex">
        <div class="flex-shrink-0">
            <svg class="h-5 w-5 text-green-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                <path fill-rule="evenodd" d="M6.267 3.455a3.066 3.066 0 001.745-.723 3.066 3.066 0 013.976 0 3.066 3.066 0 001.745.723 3.066 3.066 0 012.812 2.812c.051.643.304 1.254.723 1.745a3.066 3.066 0 010 3.976 3.066 3.066 0 00-.723 1.745 3.066 3.066 0 01-2.812 2.812 3.066 3.066 0 00-1.745.723 3.066 3.066 0 01-3.976 0 3.066 3.066 0 00-1.745-.723 3.066 3.066 0 01-2.812-2.812 3.066 3.066 0 00-.723-1.745 3.066 3.066 0 010-3.976 3.066 3.066 0 00.723-1.745 3.066 3.066 0 012.812-2.812zm7.44 5.252a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
            </svg>
        </div>
        <div class="ml-3">
            <h3 class="text-sm leading-5 font-medium text-green-800">
                {{ session('success') }}
            </h3>
        </div>
        </div>
    </div>
    @endif
  </div>
  @if(!$isSaved)
    <div class="mt-6 grid grid-cols-1 gap-y-3 gap-x-2 sm:grid-cols-12">

        <x-input.group label="1. Filer Code/Entry Number" for="entry_no" col="sm:col-span-3">
            <x-input.text wire:model="entry.entry_no" id="entry_no" placeholder="C51-1234567-1" :error="$errors->first('entry.entry_no')"  />
        </x-input>

        <x-input.group label="2. Entry Type" for="entry_type" col="sm:col-span-6">
            <x-input.text wire:model="entry.entry_type" id="entry_type" :error="$errors->first('entry.entry_type')" />
        </x-input>

        <x-input.group label="3. Summary Date" for="summary_date" col="sm:col-span-3">
            <x-input.date wire:model="entry.summary_date" id="summary_date" placeholder="mm/dd/yyyy" :error="$errors->first('entry.summary_date')"   />
        </x-input>

        <x-input.group label="4. Surety Number" for="surety_no" col="sm:col-span-3">
            <x-input.text wire:model="entry.surety_no" id="surety_no" placeholder="" :error="$errors->first('entry.surety_no')"   />
        </x-input>

        <x-input.group label="5. Bond Type" for="bond_type" col="sm:col-span-4">
            <select wire:model="bond_type" id="bond_type" class="form-select block w-full transition duration-150 ease-in-out sm:text-xs sm:leading-5">
            <option value="">- None -</option>
            <option value="0">0 - U.S. Government or entry types not requiring a bond</option>
            <option value="8">8 - Continuous</option>
            <option value="9">9 - Single Transaction</option>
            </select>
        </x-input>

        <x-input.group label="6. Port Code" for="port_code" col="sm:col-span-2">
            <x-input.text wire:model="entry.port_code" id="port_code" placeholder="" :error="$errors->first('entry.port_code')" />
        </x-input>

        <x-input.group label="7. Entry Date" for="entry_date" col="sm:col-span-3">
            <x-input.date wire:model="entry.entry_date" id="entry_date" placeholder="mm/dd/yyyy" :error="$errors->first('entry.entry_date')"   />
        </x-input>

        <x-input.group label="8. Importing Carrier" for="importing_carrier" col="sm:col-span-3">
            <x-input.text wire:model="entry.importing_carrier" id="importing_carrier" placeholder="" :error="$errors->first('entry.importing_carrier')"  />
        </x-input>

        <x-input.group label="9. Mode of Transport" for="mode_of_transport" col="sm:col-span-6">
            <x-input.select wire:model="entry.mode_of_transport" id="mode_of_transport">
                <option value="">- None -</option>
                <option value="10">10 - Vessel - non-container</option>
                <option value="11">11 - Vessel, container</option>
                <option value="12">12 - Border, Waterborne</option>
                <option value="20">20 - Rail, non-container</option>
                <option value="21">21 - Rail, container</option>
                <option value="30">30 - Truck, non-container</option>
                <option value="31">31 - Truck, container</option>
                <option value="32">32 - Auto</option>
                <option value="33">33 - Pedestrian</option>
                <option value="34">34 - Road, other</option>
                <option value="40">40 - Air, non-container</option>
                <option value="41">41 - Air, container</option>
                <option value="50">50 - Mail</option>
                <option value="60">60 - Passenger, hand-carried</option>
                <option value="70">70 - Fixed transport installation (includes pipelines, powerhouse, etc.)</option>
            </x-input>
        </x-input>

        <x-input.group label="11. Import Date" for="import_date" col="sm:col-span-3">
            <x-input.date wire:model="entry.import_date" id="import_date" placeholder="mm/dd/yyyy" :error="$errors->first('entry.import_date')"   />
        </x-input>

        <x-input.group label="10. Country of Origin" for="country_of_origin" col="sm:col-span-2">
            <x-input.text wire:model="entry.country_of_origin" id="country_of_origin" placeholder="" :error="$errors->first('entry.country_of_origin')"   />
        </x-input>

        <x-input.group label="12. B/L or AWB Number" for="bill_of_lading_awb_no" col="sm:col-span-2">
            <x-input.text wire:model="entry.bill_of_lading_awb_no" id="bill_of_lading_awb_no" placeholder="" :error="$errors->first('entry.bill_of_lading_awb_no')"  />
        </x-input>

        <x-input.group label="13. Manufacturer ID" for="manufacturer_id" col="sm:col-span-2">
            <x-input.text wire:model="entry.manufacturer_id" id="manufacturer_id" placeholder="" :error="$errors->first('entry.manufacturer_id')" />
        </x-input>

        <x-input.group label="14. Exporting Country" for="exporting_country" col="sm:col-span-3">
            <select wire:model="entry.exporting_country" id="exporting_country" class="form-select block w-full transition duration-150 ease-in-out sm:text-xs sm:leading-5">
                <option value="United States">United States</option>
                <option value="Canada">Canada</option>
                <option value="Mexico">Mexico</option>
            </select>
        </x-input>

        <x-input.group label="15. Export Date" for="export_date" col="sm:col-span-3">
            <x-input.date wire:model="entry.export_date" id="export_date" placeholder="mm/dd/yyyy" :error="$errors->first('entry.export_date')" />
        </x-input>

        <x-input.group label="16. I.T. Number" for="it_no" col="sm:col-span-3">
            <x-input.text wire:model="entry.it_no" id="it_no" placeholder="" />
        </x-input>

        <x-input.group label="17. I.T. Date" for="it_date" col="sm:col-span-3" helptext="If three or more documents are missing, record the code number for the first document and insert code '99' to indicate more than one additional document is missing.">
            <x-input.date wire:model="entry.it_date" id="it_date" placeholder="mm/dd/yyyy" />
        </x-input>

        <x-input.group label="18. Missing Docs" for="missing_documents" col="sm:col-span-6">
            <select id="missing_documents" class="form-select block w-full transition duration-150 ease-in-out sm:text-xs sm:leading-5">
                <option value="">- Select -</option>
                <option value="01">01 - Commercial Invoice</option>
                <option value="10">10 - CBP FORM 5523 (19 C.F.R. § 141.89) (Optional for footwear)</option>
                <option value="16">16 - Corrected Commercial Invoice (19 C.F.R. § 141.89, et al)</option>
                <option value="17">17 - Other Agency Form (19 C.F.R. § Part 12)</option>
                <option value="19">19 - Scale weight</option>
                <option value="21">21 - Coffee Form O</option>
                <option value="22">22 - Chemical Analysis</option>
                <option value="23">23 - Outturn Report</option>
                <option value="26">26 - Packing List (19 C.F.R. § 141.86(e))</option>
                <option value="98">98 - Not Specified Above</option>
                <option value="99">99 - More than one additional document is missing.</option>
                <option value="14">14 - Lease Statements (19 C.F.R. § 10.108)</option>
                <option value="15">15 - Re Melting Certificate (19 C.F.R. § 54.6(a))</option>
                <option value="18">18 - Duty Free Entry Certificate (19 C.F.R. § 10.102; 9808.00.3000)</option>
                <option value="20">20 - End Use Certificate (19 C.F.R. § 10.138)</option>
            </select>
        </x-input>

        <x-input.group label="19. Foreign Port of Lading" for="foreign_port_of_lading" col="sm:col-span-3">
            <x-input.text wire:model="entry.foreign_port_of_lading" id="foreign_port_of_lading" placeholder="" />
        </x-input>

        <x-input.group label="20. U.S. Port of Unlading" for="us_port_of_lading" col="sm:col-span-3">
            <x-input.text wire:model="entry.us_port_of_lading" id="us_port_of_lading" placeholder="" />
        </x-input>

        <x-input.group label="21. Location of Goods/G.O. Number" for="location_of_goods_go_no" col="sm:col-span-3">
            <x-input.text wire:model="entry.location_of_goods_go_no" id="location_of_goods_go_no" placeholder="" />
        </x-input>

        <x-input.group label="22. Consignee" for="consignee" col="sm:col-span-6">
            <x-input.text autocomplete="off" wire:model="searchConsignee" id="consignee" placeholder="" :error="$errors->first('entry.consignee')" />
            <x-input.hidden wire:model="entry.consignee" id="consignee" placeholder="" :error="$errors->first('entry.consignee')" />
            <a href="{{ route('importer.create') }}" class="absolute right-0 text-blue-700" style="top: 5px; right: 4px;">
                <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </a>
            @if(count($consignees) > 0 && !$isSaved)
                <div class="absolute mt-1 w-full rounded-md bg-white shadow-lg z-10">
                    <ul tabindex="-1" role="listbox" aria-labelledby="listbox-label" aria-activedescendant="listbox-item-3" class="max-h-56 rounded-md py-1 text-base leading-6 shadow-xs overflow-auto focus:outline-none sm:text-sm sm:leading-5">
                        @foreach($consignees as $consignee)
                                <li wire:key="{{ $loop->index }}" wire:click="consigneeSelected({{ $consignee->id }})" id="listbox-item-0" role="option" class="hover:text-indigo-600 cursor-pointer text-gray-900 cursor-default select-none relative py-2 pl-3 pr-9">
                                    <div class="flex items-center space-x-3">
                                        <span class="@if($entry->consignee == $consignee->id) font-semibold @else font-normal @endif block truncate">
                                            {{ $consignee->name }}
                                        </span>
                                        @if($consignee->importer_no)
                                            <span class="@if($entry->consignee == $consignee->id) font-semibold @else font-normal @endif block truncate">
                                                ({{ $consignee->importer_no }})
                                            </span>
                                        @endif
                                    </div>
                                    @if($entry->consignee == $consignee->id)
                                        <span class="absolute inset-y-0 right-0 flex items-center pr-4">
                                            <!-- Heroicon name: check -->
                                            <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                            </svg>
                                        </span>
                                    @endif
                                </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </x-input>

        <x-input.group label="23. Importer" for="importer" col="sm:col-span-6">
            <x-input.text autocomplete="off" wire:model="searchImporter" id="importerSearch" placeholder="" :error="$errors->first('entry.importer')" />
            <x-input.hidden wire:model="entry.importer"  id="importer" placeholder="" :error="$errors->first('entry.importer')" />
            <a href="{{ route('importer.create') }}" class="absolute right-0 text-blue-700" style="top: 5px; right: 4px;">
                <svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
            </a>
            @if(count($importers) > 0)
                <div class="absolute mt-1 w-full rounded-md bg-white shadow-lg z-10">
                    <ul tabindex="-1" role="listbox" aria-labelledby="listbox-label" aria-activedescendant="listbox-item-3" class="max-h-56 rounded-md py-1 text-base leading-6 shadow-xs overflow-auto focus:outline-none sm:text-sm sm:leading-5">
                        @foreach($importers as $importer)
                                <li wire:key="{{ $loop->index }}" wire:click="importerSelected({{ $importer->id }})" id="listbox-item-0" role="option" class="hover:text-indigo-600 cursor-pointer text-gray-900 cursor-default select-none relative py-2 pl-3 pr-9">
                                    <div class="flex items-center space-x-3">
                                        <span class="@if($entry->importer == $importer->id) font-semibold @else font-normal @endif block truncate">
                                            {{ $importer->name }}
                                        </span>
                                        @if($importer->importer_no)
                                            <span class="@if($entry->importer == $importer->id) font-semibold @else font-normal @endif block truncate">
                                                ({{ $importer->importer_no }})
                                            </span>
                                        @endif
                                    </div>
                                    @if($entry->importer == $importer->id)
                                        <span class="absolute inset-y-0 right-0 flex items-center pr-4">
                                            <!-- Heroicon name: check -->
                                            <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                            </svg>
                                        </span>
                                    @endif
                                </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </x-input>

        <x-input.group label="24. Reference Number" for="reference_no" col="sm:col-span-5">
            <x-input.text wire:model="entry.reference_no" id="reference_no" placeholder="" />
        </x-input>

        <x-input.group label="Packaging" for="packaging" col="sm:col-span-5">
            <x-input.text wire:model="entry.packaging" id="packaging" placeholder="" />
        </x-input>

        <x-input.group label="Related" for="related" col="sm:col-span-2">
            <select wire:model="entry.related" id="related" class="form-select block w-full transition duration-150 ease-in-out sm:text-xs sm:leading-5">
                <option>- Select -</option>
                <option value="Not Related">Not Related</option>
                <option value="Related">Related</option>
            </select>
        </x-input>

        <x-input.group label="41. Declarant" for="declarant" col="sm:col-span-6">
            <x-input.text wire:model="entry.declarant" id="declarant" placeholder="" :error="$errors->first('entry.declarant')" />
        </x-input>

        <x-input.group label="41. Declarant" for="declarant_title" col="sm:col-span-3">
            <x-input.text wire:model="entry.declarant_title" id="declarant_title" placeholder="" :error="$errors->first('entry.declarant_title')" />
        </x-input>

        <x-input.group label="41. Filed Date" for="filed_date" col="sm:col-span-3">
            <x-input.date wire:model="entry.filed_date" id="filed_date" placeholder="" />
        </x-input>

    </div>
    <div class="m-8 border-t border-gray-200 pt-5">
        <div class="flex justify-end">
        <span class="inline-flex rounded-md shadow-sm">
            <button type="button" class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
            Cancel
            </button>
        </span>
        <span class="ml-3 inline-flex rounded-md shadow-sm">
            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
            Save
            </button>
        </span>
        </div>
    </div>
  @else
    <div class="mt-6 grid grid-cols-1 gap-y-3 gap-x-2 sm:grid-cols-12">

        <x-input.group label="1. Filer Code/Entry Number" for="entry_no" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->entry_no }}</p>
        </x-input>

        <x-input.group label="2. Entry Type" for="entry_type" col="sm:col-span-6">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->entry_type }}</p>
        </x-input>

        <x-input.group label="3. Summary Date" for="summary_date" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->summary_date }}</p>
        </x-input>

        <x-input.group label="4. Surety Number" for="surety_no" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->surety_no }}</p>
        </x-input>

        <x-input.group label="5. Bond Type" for="bond_type" col="sm:col-span-4">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->bond_type }}</p>
        </x-input>

        <x-input.group label="6. Port Code" for="port_code" col="sm:col-span-2">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->port_code }}</p>
        </x-input>

        <x-input.group label="7. Entry Date" for="entry_date" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->entry_date }}</p>
        </x-input>

        <x-input.group label="8. Importing Carrier" for="importing_carrier" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->importing_carrier }}</p>
        </x-input>

        <x-input.group label="9. Mode of Transport" for="mode_of_transport" col="sm:col-span-6">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->mode_of_transport }}</p>
        </x-input>

        <x-input.group label="11. Import Date" for="import_date" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->import_date }}</p>
        </x-input>

        <x-input.group label="10. Country of Origin" for="country_of_origin" col="sm:col-span-2">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->country_of_origin }}</p>
        </x-input>

        <x-input.group label="12. B/L or AWB Number" for="bill_of_lading_awb_no" col="sm:col-span-2">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->bill_of_lading_awb_no }}</p>
        </x-input>

        <x-input.group label="13. Manufacturer ID" for="manufacturer_id" col="sm:col-span-2">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->manufacturer_id }}</p>
        </x-input>

        <x-input.group label="14. Exporting Country" for="exporting_country" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->exporting_country }}</p>
        </x-input>

        <x-input.group label="15. Export Date" for="export_date" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->export_date }}</p>
        </x-input>

        <x-input.group label="16. I.T. Number" for="it_no" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->it_no }}</p>
        </x-input>

        <x-input.group label="17. I.T. Date" for="it_date" col="sm:col-span-3" helptext="If three or more documents are missing, record the code number for the first document and insert code '99' to indicate more than one additional document is missing.">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->it_date }}</p>
        </x-input>

        <x-input.group label="18. Missing Docs" for="missing_documents" col="sm:col-span-6">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->missing_documents }}</p>
        </x-input>

        <x-input.group label="19. Foreign Port of Lading" for="foreign_port_of_lading" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->foreign_port_of_lading }}</p>
        </x-input>

        <x-input.group label="20. U.S. Port of Unlading" for="us_port_of_lading" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->us_port_of_lading }}</p>
        </x-input>

        <x-input.group label="21. Location of Goods/G.O. Number" for="location_of_goods_go_no" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->location_of_goods_go_no }}</p>
        </x-input>

        <div for="consignee" col="sm:col-span-3" class="sm:col-span-6">
            <label class="block text-xs font-medium leading-5 text-gray-700">Consignee</label>
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->consignee }}</p>
        </div>

        <div for="importer" class="sm:col-span-6">
            <label class="block text-xs font-medium leading-5 text-gray-700">Importer</label>
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->importer }}</p>
        </div>

        <x-input.group label="24. Reference Number" for="reference_no" col="sm:col-span-5">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->reference_no }}</p>
        </x-input>

        <x-input.group label="Packaging" for="packaging" col="sm:col-span-5">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->packaging }}</p>
        </x-input>

        <x-input.group label="Related" for="related" col="sm:col-span-2">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->related }}</p>
        </x-input>

        <x-input.group label="41. Declarant" for="declarant" col="sm:col-span-6">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->declarant }}</p>
        </x-input>

        <x-input.group label="41. Declarant" for="declarant_title" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->declarant_title }}</p>
        </x-input>

        <x-input.group label="41. Filed Date" for="filed_date" col="sm:col-span-3">
            <p class="text-gray-700 font-semibold text-xl">{{ $entry->filed_date }}</p>
        </x-input>

    </div>
    <div class="m-8 border-t border-gray-200 pt-5">
        <div class="flex justify-end">
        <span class="inline-flex rounded-md shadow-sm">
        <a wire:click.prevent="cancel" class="py-2 cursor-pointer px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
                Cancel
            </a>
        </span>
        <span class="ml-3 inline-flex rounded-md shadow-sm">
            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
            Save
            </button>
        </span>
        </div>
    </div>
  @endif
</form>
