<form wire:submit.prevent="save">
    <div class="errors">
        @if ($errors->any())
            <div class="rounded-md bg-red-50 p-4">
                <div class="flex">
                <div class="flex-shrink-0">
                    <svg class="h-5 w-5 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" clip-rule="evenodd" />
                    </svg>
                </div>
                <div class="ml-3">
                    <h3 class="text-sm leading-5 font-medium text-red-800">
                    There were errors with your submission
                    </h3>
                    <div class="mt-2 text-sm leading-5 text-red-700">
                    <ul class="list-disc pl-5">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    </div>
                </div>
                </div>
            </div>
        @endif

        @if(session('success'))
        <div class="rounded-md bg-green-50 p-4">
            <div class="flex">
            <div class="flex-shrink-0">
                <svg class="h-5 w-5 text-green-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M6.267 3.455a3.066 3.066 0 001.745-.723 3.066 3.066 0 013.976 0 3.066 3.066 0 001.745.723 3.066 3.066 0 012.812 2.812c.051.643.304 1.254.723 1.745a3.066 3.066 0 010 3.976 3.066 3.066 0 00-.723 1.745 3.066 3.066 0 01-2.812 2.812 3.066 3.066 0 00-1.745.723 3.066 3.066 0 01-3.976 0 3.066 3.066 0 00-1.745-.723 3.066 3.066 0 01-2.812-2.812 3.066 3.066 0 00-.723-1.745 3.066 3.066 0 010-3.976 3.066 3.066 0 00.723-1.745 3.066 3.066 0 012.812-2.812zm7.44 5.252a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                </svg>
            </div>
            <div class="ml-3">
                <h3 class="text-sm leading-5 font-medium text-green-800">
                    {{ session('success') }}
                </h3>
            </div>
            </div>
        </div>
        @endif
    </div>
    <div class="mt-6 grid grid-cols-1 gap-y-3 gap-x-2 sm:grid-cols-12">
        <x-input.group label="Name" for="name" col="sm:col-span-6">
            <x-input.text wire:model="name" id="name" placeholder="Name" :error="$errors->first('name')"  />
        </x-input>
        <x-input.group label="Name 2" for="name_two" col="sm:col-span-6">
            <x-input.text wire:model="name_two" id="name_two" placeholder="Name 2" :error="$errors->first('name_two')"  />
        </x-input>
        <x-input.group label="Importer No." for="importer_no" col="sm:col-span-5">
            <x-input.text wire:model="importer_no" id="importer_no" placeholder="Importer No" :error="$errors->first('importer_no')"  />
        </x-input>
        <x-input.group label="Surety No." for="surety_no" col="sm:col-span-2">
            <x-input.text wire:model="surety_no" id="surety_no" placeholder="Surety No" :error="$errors->first('surety_no')"  />
        </x-input>
        <x-input.group label="Bond Type" for="bond_type" col="sm:col-span-5">
            <select wire:model="bond_type" id="bond_type" class="form-select block w-full transition duration-150 ease-in-out sm:text-xs sm:leading-5" >
                <option >-- Select Bond Type --</option>
                <option value="0">0 - U.S. Government or entry types not requiring a bond</option>
                <option value="8">8 - Continuous</option>
                <option value="9">9 - Single Transaction</option>
            </select>
        </x-input>
        <x-input.group label="Country" for="country" col="sm:col-span-4">
            <x-input.text wire:model="country" id="country" placeholder="Country" :error="$errors->first('country')"  />
        </x-input>
        <x-input.group label="Address" for="address" col="sm:col-span-8">
            <x-input.text wire:model="address" id="address" placeholder="Address" :error="$errors->first('address')"  />
        </x-input>
        <x-input.group label="Address 2" for="address_two" col="sm:col-span-12">
            <x-input.text wire:model="address_two" id="address_two" placeholder="Address 2" :error="$errors->first('address_two')"  />
        </x-input>
        <x-input.group label="City" for="city" col="sm:col-span-4">
            <x-input.text wire:model="city" id="city" placeholder="City" :error="$errors->first('city')"  />
        </x-input>
        <x-input.group label="State" for="state" col="sm:col-span-4">
            <x-input.text wire:model="state" id="state" placeholder="State" :error="$errors->first('state')"  />
        </x-input>
        <x-input.group label="Postal Code" for="postal_code" col="sm:col-span-4">
            <x-input.text wire:model="postal_code" id="postal_code" placeholder="Postal Code" :error="$errors->first('postal_code')"  />
        </x-input>
    </div>
    <div class="m-8 border-t border-gray-200 pt-5">
      <div class="flex justify-end">
        <span class="inline-flex rounded-md shadow-sm">
            <a href="{{ route('entry') }}" type="button" class="py-2 px-4 border border-gray-300 rounded-md text-sm leading-5 font-medium text-gray-700 hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800 transition duration-150 ease-in-out">
            Cancel
          </a>
        </span>
        <span class="ml-3 inline-flex rounded-md shadow-sm">
          <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
            Save
          </button>
        </span>
      </div>
    </div>
  </form>
