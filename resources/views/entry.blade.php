
<x-app-layout>
<!--
  Tailwind UI components require Tailwind CSS v1.8 and the @tailwindcss/ui plugin.
  Read the documentation to get started: https://tailwindui.com/documentation
-->
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @if(isset($entry_no) && $entry_no != "")
                {{ __('Edit Entry') }} {{ $entry_no }}
            @else
                {{ __('Create Entry') }}
            @endif
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                  <div class="p-6 sm:px-20">
                    <div class="mt-8">
                      <livewire:entry-summary :entry_no="$entry_no" />
                    </div>
                    <div class="mt-8 border-t border-gray-200 pt-8">
                      <div>
                        <h3 class="text-lg leading-6 font-medium text-gray-900">
                          Invoices
                        </h3>
                        <p class="mt-1 text-sm leading-5 text-gray-500">
                          We'll always let you know about important changes, but you pick what else you want to hear about.
                        </p>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>

    <script>
        // document.addEventListener('click', function(){
        //     Livewire.emit('clearDropDown')
        // })
    </script>
</x-app-layout>
