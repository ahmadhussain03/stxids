
<x-app-layout>
    <!--
      Tailwind UI components require Tailwind CSS v1.8 and the @tailwindcss/ui plugin.
      Read the documentation to get started: https://tailwindui.com/documentation
    -->
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Edit User') }}
            </h2>
        </x-slot>

        <div class="py-12">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                      <div class="p-6 sm:px-20">
                        <div class="mt-8">
                          <livewire:edit-user :user="$user"  />
                        </div>
                      </div>
                </div>
            </div>
        </div>

    </x-app-layout>
