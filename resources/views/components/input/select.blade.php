<select
    {{ $attributes }}
    class="form-select block w-full transition duration-150 ease-in-out sm:text-xs sm:leading-5">
    {{ $slot }}
</select>
