

<input
    {{ $attributes }}
    type="hidden"
    class="form-input block w-full transition duration-150 ease-in-out sm:text-sm sm:leading-5 {{ $error ? 'border-red-300 text-red-900 placeholder-red-300 focus:border-red-300 focus:shadow-outline-red' : '' }}">
