@props([
    'label',
    'for',
    'col'
])

<div class="{{ $col }}">
  <label
      for="{{ $for }}"
      class="block text-xs font-medium leading-5 text-gray-700"
      >
    {{ $label }}
  </label>
  <div
      class="mt-1 relative rounded-md shadow-sm">

      {{ $slot }}

  </div>
</div>
