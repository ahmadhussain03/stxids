
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Term Detail') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-6 sm:px-20">
                    <div class="mt-8">
                        <div class="flex justify-start">
                            <span class="inline-flex rounded-md shadow-sm">
                                <a href="{{ route('law.edit', ['id' => $law->id]) }}" class="inline-flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-500 focus:outline-none focus:border-indigo-700 focus:shadow-outline-indigo active:bg-indigo-700 transition duration-150 ease-in-out">
                                    Edit
                                </a>
                            </span>
                            <span class="ml-3 inline-flex rounded-md shadow-sm">
                                <a href="{{ url()->previous() }}" class="inline-flex justify-center py-2 px-4 border border-transparent text-sm leading-5 font-medium rounded-md text-white bg-red-600 hover:bg-red-500 focus:outline-none focus:border-red-700 focus:shadow-outline-red active:bg-red-700 transition duration-150 ease-in-out">
                                    Go Back
                                </a>
                            </span>
                          </div>
                        <div class="mt-6 grid grid-cols-1 gap-y-3 gap-x-2 sm:grid-cols-12">
                            <x-input.group label="Name" for="name" col="sm:col-span-6">
                                <p class="text-gray-700 font-semibold text-xl">{{ $law->name }}</p>
                            </x-input>
                            <x-input.group label="Description" for="description" col="sm:col-span-6">
                                <p class="text-gray-700 font-semibold text-xl">{{ $law->description }}</p>
                            </x-input>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
