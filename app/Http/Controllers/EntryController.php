<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EntryController extends Controller
{
    public $entry_no;

    public function view($entry_no) {
        $this->entry_no = $entry_no;

        return view('entry', [
            'entry_no' => compact($this->entry_no)
        ]);
    }

    public function create() {
        return view('entry', [
            'entry_no' => $this->entry_no
        ]);
    }
}
