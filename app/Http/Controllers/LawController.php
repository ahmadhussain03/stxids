<?php

namespace App\Http\Controllers;

use App\Models\Law;
use Illuminate\Http\Request;

class LawController extends Controller
{
    public function create()
    {
        return view('law.create');
    }

    public function edit($id)
    {
        $law = Law::findOrFail($id);
        return view('law.edit', ['law' => $law]);
    }

    public function show($id)
    {
        $law = Law::findOrFail($id);
        return view('law.show', ['law' => $law]);
    }

}
