<?php

namespace App\Http\Controllers;

use App\Models\Importer;
use Illuminate\Http\Request;

class ImporterController extends Controller
{
    public function create()
    {
        return view('importer.create');
    }

    public function edit($id)
    {
        $importer = Importer::findOrFail($id);
        return view('importer.edit', ['importer' => $importer]);
    }

    public function show($id)
    {
        $importer = Importer::findOrFail($id);
        return view('importer.show', ['importer' => $importer]);
    }
}
