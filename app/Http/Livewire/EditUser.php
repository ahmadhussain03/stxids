<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EditUser extends Component
{

    public $user;

    protected $rules = [
        'user.name' => 'required|string|min:6|max:255',
        'user.email' => 'required|email|unique:users,email',
        'user.password_field' => 'nullable|max:255|min:6|string',
        'user.expire_at_field' => 'nullable|date',
        'user.level' => 'required|integer|in:1,2,3'
    ];

    public function update()
    {
        $this->validate([
            'user.name' => 'required|string|min:2|max:255',
            'user.email' => 'required|email|unique:users,email,' . $this->user->id,
            'user.password_field' => 'nullable|max:255|min:6|string',
            'user.expire_at_field' => 'nullable|date',
            'user.level' => 'required|integer|in:1,2,3'
        ]);

        $expireField = $this->user->expire_at_field;
        unset($this->user->expire_at_field);
        if($this->user->password_field){
            $this->user->password = bcrypt($this->user->password_field);
            unset($this->user->password_field);
        } else {
            unset($this->user->password);
            unset($this->user->password_field);
        }

        $this->user->update([
            'expire_at' => $expireField
        ]);

        $this->user->save();
        session()->flash('success', 'User Updated Successfully!');
        $this->user->password_field = "";
        if($this->user->expire_at)
            $this->user->expire_at_field = $this->user->expire_at->format('Y-m-d');
        else
            $this->user->expire_at_field = "";
    }

    public function mount($user)
    {
        $this->user = $user;
        if($this->user->expire_at)
            $this->user->expire_at_field = $this->user->expire_at->format('Y-m-d');
        else
            $this->user->expire_at_field = "";
        $this->user->password_field = "";
    }

    public function render()
    {
        return view('livewire.edit-user');
    }
}
