<?php

namespace App\Http\Livewire;
use App\Models\Entry;
use App\Models\Importer;
use Livewire\Component;

class EntrySummary extends Component
{
    public $entry_no, $searchImporter, $isSaved = false, $searchConsignee, $isSearchingImporter = false, $isSearchingConsignee = false; // passed in from the controller
    public $entry;

    protected $listeners = ['clearDropDown'];

    protected $rules = [
        'entry.entry_no' => 'bail|required|string|min:13|unique:entries,entry_no',
        'entry.entry_type' => 'numeric|max:2',
        'entry.summary_date' => 'nullable|date',
        'entry.surety_no' => 'nullable|numeric',
        'entry.bond_type' => 'nullable|numeric',
        'entry.port_code' => 'nullable|numeric',
        'entry.entry_date' => 'required|date',
        'entry.importing_carrier' => 'nullable',
        'entry.mode_of_transport' => 'required',
        'entry.country_of_origin' => 'nullable',
        'entry.import_date' => 'nullable',
        'entry.bill_of_lading_awb_no' => 'nullable',
        'entry.manufacturer_id' => 'nullable',
        'entry.exporting_country' => 'nullable',
        'entry.export_date' => 'nullable|date',
        'entry.it_no' => 'nullable',
        'entry.it_date' => 'nullable',
        'entry.missing_documents' => 'nullable',
        'entry.foreign_port_of_lading' => 'nullable',
        'entry.us_port_of_lading' => 'nullable',
        'entry.location_of_goods_go_no' => 'nullable',
        'entry.consignee' => 'required',
        'entry.importer' => 'required',
        'entry.reference_no' => 'nullable',
        'entry.packaging' => 'nullable',
        'entry.related' => 'nullable',
        'entry.declarant' => 'required',
        'entry.declarant_title' => 'required',
        'entry.filed_date' => 'nullable|date',
        'entry.entered_value_total' => 'nullable|numeric',
        'entry.duty_total' => 'nullable|numeric'
    ];

    protected $attributes = [
        'entry.mode_of_transport' => 11,
    ];

    public function clearDropDown()
    {
        if($this->isSaved){
            dd($this->isSaved);
            return;
        }
        if($this->isSearchingConsignee){
            $consignee = Importer::find($this->entry->consignee);
            if($consignee){
                $this->searchConsignee = $consignee->name . '(' . $consignee->importer_no . ')';
            } else {
                $this->searchConsignee = null;
            }
        }

        if($this->isSearchingImporter){
            $importer = Importer::find($this->entry->importer);
            if($importer){
                $this->searchImporter = $importer->name . '(' . $importer->importer_no . ')';
            } else {
                $this->searchImporter = null;
            }
        }

        $this->isSearchingImporter = false;
        $this->isSearchingConsignee = false;

    }

    public function updatedSearchConsignee($value)
    {
        if($value)
            $this->isSearchingConsignee = true;
        else
            $this->isSearchingConsignee = false;
    }

    public function updatedSearchImporter($value)
    {
        if($value)
            $this->isSearchingImporter = true;
        else
            $this->isSearchingImporter = false;
    }

    public function consigneeSelected($id)
    {
        $consignee = Importer::findOrFail($id);
        $this->entry->consignee = $consignee->id;
        $this->searchConsignee = $consignee->name . '(' . $consignee->importer_no . ')';
        $this->isSearchingConsignee = false;
    }

    public function importerSelected($id)
    {
        $importer = Importer::findOrFail($id);
        $this->entry->importer = $importer->id;
        $this->searchImporter = $importer->name . '(' . $importer->importer_no . ')';
        $this->isSearchingImporter = false;
    }

    public function mount() {

        if(isset($this->entry_no)) $entry = Entry::find($this->entry_no);
        $this->entry = $this->entry ?? new Entry;
        // $this->entry->entry_no = $this->entry->entry_no ?? "C51-1234567-0";
        // $this->entry->mode_of_transport = $this->entry->mode_of_transport ?? 11;

    }

    public function render()
    {
        $consignees = $this->isSearchingConsignee ? Importer::where('name', 'like', '%' . $this->searchConsignee . '%')->orWhere('importer_no', 'like', '%' . $this->searchConsignee . '%')->paginate(5) : [];
        $importers = $this->isSearchingImporter ? Importer::where('name', 'like', '%' . $this->searchImporter . '%')->orWhere('importer_no', 'like', '%' . $this->searchImporter . '%')->paginate(5) : [];

        return view('livewire.entry-summary', ['consignees' => $consignees, 'importers' => $importers]);
    }

    public function save()
    {
        $this->validate();
        if($this->isSaved){
            $this->entry->save();
            session()->flash('success', 'Entry Created Successfully!');
            $this->entry = new Entry;
            $this->isSaved = false;
        } else {
            $this->isSaved = true;
        }

    }

    public function cancel()
    {
        $this->isSaved = false;
    }
}
