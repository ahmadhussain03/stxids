<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EditImporter extends Component
{

    public $importer;

    protected $rules = [
        'importer.name' => 'required|string|max:255|min:2',
        'importer.name_two' => 'nullable|string|max:255',
        'importer.importer_no' => 'nullable|string|max:255',
        'importer.surety_no' => 'nullable|string|max:255',
        'importer.bond_type' => 'nullable|integer|in:0,8,9',
        'importer.country' => 'nullable|string|max:255',
        'importer.address' => 'required|string|max:255',
        'importer.address_two' => 'nullable|string|max:255',
        'importer.city' => 'required|string|max:255',
        'importer.state' => 'required|string|max:255',
        'importer.postal_code' => 'required|string|max:255',
    ];

    public function mount($importer)
    {
        $this->importer = $importer;
    }

    public function update()
    {
        $this->importer->save();
        session()->flash('success', 'Customer Updated Successfully!');
    }

    public function render()
    {
        return view('livewire.edit-importer');
    }
}
