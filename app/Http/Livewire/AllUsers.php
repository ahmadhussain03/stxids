<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class AllUsers extends Component
{
    use WithPagination;

    public function render()
    {
        return view('livewire.all-users', [
            'users' => User::paginate(10)
        ]);
    }
}
