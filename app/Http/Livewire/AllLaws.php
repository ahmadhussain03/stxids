<?php

namespace App\Http\Livewire;

use App\Models\Law;
use Livewire\Component;
use Livewire\WithPagination;

class AllLaws extends Component
{

    use WithPagination;

    public function render()
    {
        return view('livewire.all-laws', [
            'laws' => Law::paginate(10)
        ]);
    }
}
