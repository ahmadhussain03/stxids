<?php

namespace App\Http\Livewire;

use App\Models\Importer;
use Livewire\Component;
use Livewire\WithPagination;

class AllImporters extends Component
{

    use WithPagination;

    public function render()
    {
        return view('livewire.all-importers', [
            'importers' => Importer::paginate(10)
        ]);
    }
}
