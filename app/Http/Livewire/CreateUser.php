<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class CreateUser extends Component
{

    public $name, $email, $password, $expire, $level;

    protected $rules = [
        'name' => 'required|min:6|max:255|string',
        'email' => 'required|email|unique:users',
        'password' => 'required|min:6|max:255|string',
        'expire' => 'nullable|date',
        'level' => 'required|integer|in:1,2,3'
    ];

    public function save()
    {
        $this->validate();

        $user = User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => bcrypt($this->password),
            'expire_at' => $this->expire,
            'level' => $this->level
        ]);

        session()->flash('success', 'User Created Successfully!');
    }

    public function render()
    {
        return view('livewire.create-user');
    }
}
