<?php

namespace App\Http\Livewire;

use App\Models\Law;
use Livewire\Component;

class CreateLaw extends Component
{

    public $name, $description;

    protected $rules = [
        'name' => 'required|string|max:255',
        'description' => 'nullable|string'
    ];

    public function render()
    {
        return view('livewire.create-law');
    }

    public function save()
    {
        $this->validate();

        Law::create([
            'name' => $this->name,
            'description' => $this->description
        ]);

        session()->flash('success', 'Law Added Successfully');
    }
}
