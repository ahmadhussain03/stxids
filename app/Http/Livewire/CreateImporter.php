<?php

namespace App\Http\Livewire;

use App\Models\Importer;
use Livewire\Component;

class CreateImporter extends Component
{

    public $name, $name_two, $importer_no, $surety_no, $bond_type, $country, $address, $address_two, $city, $state, $postal_code;

    protected $rules = [
        'name' => 'required|string|max:255|min:2',
        'name_two' => 'nullable|string|max:255',
        'importer_no' => 'nullable|string|max:255',
        'surety_no' => 'nullable|string|max:255',
        'bond_type' => 'nullable|integer|in:0,8,9',
        'country' => 'nullable|string|max:255',
        'address' => 'required|string|max:255',
        'address_two' => 'nullable|string|max:255',
        'city' => 'required|string|max:255',
        'state' => 'required|string|max:255',
        'postal_code' => 'required|string|max:255',
    ];

    public function save()
    {
        $this->validate();

        Importer::create([
            'name' => $this->name,
            'name_two' => $this->name_two,
            'importer_no' => $this->importer_no,
            'surety_no' => $this->surety_no,
            'bond_type' => $this->bond_type,
            'country' => $this->country,
            'address' => $this->address,
            'address_two' => $this->address_two,
            'city' => $this->city,
            'state' => $this->state,
            'postal_code' => $this->postal_code,
        ]);

        session()->flash('success', 'Customer Created Successfully!');
    }

    public function render()
    {
        return view('livewire.create-importer');
    }
}
