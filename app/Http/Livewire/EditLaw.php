<?php

namespace App\Http\Livewire;

use Livewire\Component;

class EditLaw extends Component
{
    public $law;

    protected $rules = [
        'law.name' => 'required|string|max:255',
        'law.description' => 'nullable|string'
    ];

    public function mount($law)
    {
        $this->law = $law;
    }

    public function save()
    {
        $this->validate();

        $this->law->save();

        session()->flash('success', 'Term Updated Successfully!');
    }

    public function render()
    {
        return view('livewire.edit-law');
    }
}
