<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entries', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('entry_no')->nullable(); // 1
            $table->integer('entry_type')->nullable(); // 2
            $table->date('summary_date')->nullable(); // 3
            $table->string('surety_no')->nullable(); // 4
            $table->integer('bond_type')->nullable(); // 5
            $table->integer('port_code')->nullable(); // 6
            $table->date('entry_date')->nullable(); // 7
            $table->string('importing_carrier')->nullable(); // 8
            $table->integer('mode_of_transport')->nullable(); // 9 transport_no
            $table->string('country_of_origin')->nullable(); // 10
            $table->date('import_date')->nullable(); // 11
            $table->string('bill_of_lading_awb_no')->nullable(); // 12
            $table->string('manufacturer_id')->nullable(); // 13
            $table->string('exporting_country')->nullable(); // 14
            $table->date('export_date')->nullable(); // 15
            $table->string('it_no')->nullable(); // 16
            $table->date('it_date')->nullable(); // 17
            $table->integer('missing_documents')->nullable(); // 18
            $table->string('foreign_port_of_lading')->nullable(); // 19
            $table->string('us_port_of_lading')->nullable(); // 20
            $table->string('location_of_goods_go_no')->nullable(); // 21
            $table->integer('consignee')->nullable(); // 22
            $table->integer('importer')->nullable(); // 23
            $table->string('reference_no')->nullable(); // 24
            $table->string('packaging')->nullable(); // 28 container no
            $table->string('related')->nullable(); // 32 c
            $table->string('declarant')->nullable(); // 41
            $table->string('declarant_title')->nullable(); // 41
            $table->date('filed_date')->nullable(); // 41
            $table->double('entered_value_total')->nullable(); // 35
            $table->double('duty_total')->nullable(); // 37
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entries');
    }
}
